import os
from setuptools import setup

VERSION = '0.2.3'
setup(
    name = 'peegee',
    py_modules = ['peegee'],
    license = 'MIT',
    version = VERSION,
    description = 'A PostgreSQL client based on psycopg2',
    install_requires=['psycopg2-binary',],
    author = 'scku',
    author_email = 'scku208@gmail.com',
    url = 'https://gitlab.com/scku208/peegee',
    download_url = 'https://gitlab.com/scku208/peegee/-/archive/master/peegee-master.zip',
    keywords = ['postgresql', 'psycopg2', 'database', 'peegee'],
    classifiers=[
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3.10',
        ]
    )
